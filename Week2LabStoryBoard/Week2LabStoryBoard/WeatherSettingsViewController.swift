//
//  ViewController.swift
//  Week2LabStoryBoard
//
//  Created by Timothy Tan on 11/3/2023.
//

import UIKit

class WeatherSettingsViewController: UIViewController, UITextFieldDelegate, ColourChangeDelegate {
    func changeColourToColour(_ colour: UIColor) {
        colourPreviewView.backgroundColor = colour
    }
    

    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var iconSegmentedControl: UISegmentedControl!
    @IBOutlet weak var colourSegmentedControl: UISegmentedControl!
    @IBOutlet weak var colourPreviewView: UIView!
    
    @IBAction func colourSegmentValueChange(_ sender: Any) {
        var colourName = colourSegmentedControl.titleForSegment(at: colourSegmentedControl.selectedSegmentIndex) ?? ""
        if colourName != "Custom" {
            colourName = colourName.appending("Colour")
            colourPreviewView.backgroundColor = UIColor(named: colourName)
        } else {
            performSegue(withIdentifier: "pickColourSegue", sender: sender)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionTextField.delegate = self
        
        //just set it as gold cus there is no reason to need to detect what it is on launch
        colourPreviewView.backgroundColor = UIColor(named: "GoldColour")
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSummarySegue" {
            let description = descriptionTextField.text ?? ""
            var icon: WeatherIcon = .sun
            switch iconSegmentedControl.selectedSegmentIndex {
                case 1:
                    icon = .clouds
                case 2:
                    icon = .rain
                case 3:
                    icon = .lightning
                case 4:
                    icon = .snow
                default:
                    icon = .sun
            }
            
            let colour = colourPreviewView.backgroundColor?.cgColor
            
            let weatherDetails = WeatherDetails(description: description, backgroundColour: colour, icon: icon)
            let destination = segue.destination as! WeatherSummaryViewController
            destination.weatherDetails = weatherDetails
        }
        
        if segue.identifier == "pickColourSegue" {
            let destination = segue.destination as! ChooseColourViewController
            destination.initialColour = colourPreviewView.backgroundColor?.cgColor
            destination.delegate = self
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "showSummarySegue" {
            guard let description = descriptionTextField.text, description.isEmpty == true else {
                return true
            }
        }
        let alertController = UIAlertController(title: "Error", message: "Please enter a description", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

