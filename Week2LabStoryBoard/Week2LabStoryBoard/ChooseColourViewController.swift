//
//  ChooseColourViewController.swift
//  Week2LabStoryBoard
//
//  Created by Timothy Tan on 11/3/2023.
//

import UIKit

protocol ColourChangeDelegate: AnyObject {
    func changeColourToColour(_ colour: UIColor)
}

class ChooseColourViewController: UIViewController {
    
    weak var delegate: ColourChangeDelegate?
    var initialColour: CGColor?

    @IBOutlet weak var UIView: UIView!
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        let redValue = CGFloat(redSlider.value)
        let blueValue = CGFloat(blueSlider.value)
        let greenValue = CGFloat(greenSlider.value)
        
        let newColour = UIColor(red: redValue, green: greenValue, blue: blueValue, alpha: 1.0)
        UIView.backgroundColor = newColour
        
        delegate?.changeColourToColour(newColour)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //change colour of initial here
        if let colour = initialColour {
            UIView.backgroundColor = UIColor(cgColor: colour)
            
            if let colours = colour.components {
                redSlider.value = Float(colours[0])
                greenSlider.value = Float(colours[1])
                blueSlider.value = Float(colours[2])
            }
            
        }
    }
    

}
