//
//  WeatherSummaryViewController.swift
//  Week2LabStoryBoard
//
//  Created by Timothy Tan on 11/3/2023.
//

import UIKit

class WeatherSummaryViewController: UIViewController {

    @IBOutlet weak var summaryButton: UIButton!
    
    var weatherDetails: WeatherDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let weatherDetails = weatherDetails else {
            return
        }
        
        summaryButton.setTitle(weatherDetails.description, for: .normal)
        
        let buttonImage = UIImage(systemName: weatherDetails.iconImageName())
        summaryButton.setImage(buttonImage, for: .normal)
                
        if let backgroundColour = weatherDetails.backgroundColour {
            summaryButton.tintColor = UIColor(cgColor:backgroundColour)
        }
    }
    
    

}
