//
//  WeatherDetails.swift
//  Week2LabStoryBoard
//
//  Created by Timothy Tan on 11/3/2023.
//

import Foundation
import CoreGraphics

enum WeatherIcon {
    case sun
    case clouds
    case rain
    case lightning
    case snow
}

struct WeatherDetails {
    var description: String
    var backgroundColour: CGColor?
    var icon: WeatherIcon
    
    func iconImageName() -> String {
        switch icon {
            case.clouds:
                return "cloud.fill"
            case .rain:
                return "cloud.rain.fill"
            case .lightning:
                return "cloud.bolt.fill"
            case .snow:
                return "cloud.snow.fill"
        default:
            return "sun.max.fill"
        }
    }
}
